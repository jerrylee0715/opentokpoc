package com.tokbox.sample.multipartyconstraintlayout

import android.text.TextUtils

object OpenTokConfig {
    /*
    Fill the following variables using your own Project info from the OpenTok dashboard
    https://dashboard.tokbox.com/projects

    Note that this application will ignore credentials in the `OpenTokConfig` file when `CHAT_SERVER_URL` contains a
    valid URL.
    */
    /*
    Fill the following variables using your own Project info from the OpenTok dashboard
    https://dashboard.tokbox.com/projects

    Note that this application will ignore credentials in the `OpenTokConfig` file when `CHAT_SERVER_URL` contains a
    valid URL.
    */

    //  1~5 1為 PROVIDER, 2~3為 subscriber
    const val TOKEN_ID = 1

    // Replace with a API key
    const val API_KEY = "47410801"

    // Replace with a generated Session ID
    const val SESSION_ID = "1_MX40NzQxMDgwMX5-MTY0MTE5NjYzNDg5N35laEN4aHJzUkt5bkFRZ25zT0txMlpmNXl-fg"

    // Replace with a generated token (from the dashboard or using an OpenTok server SDK)

    val PROVIDER_TOKEN = arrayListOf<String>(
        "T1==cGFydG5lcl9pZD00NzQxMDgwMSZzaWc9NDBjY2JjYjQxMzYwMjJjMjdiODZlZDUzMDAzNjQ1ZjVkNWNhMGJlNjpzZXNzaW9uX2lkPTFfTVg0ME56UXhNRGd3TVg1LU1UWTBNVEU1TmpZek5EZzVOMzVsYUVONGFISnpVa3Q1YmtGUloyNXpUMHR4TWxwbU5YbC1mZyZjcmVhdGVfdGltZT0xNjQxMTk4NTA2Jm5vbmNlPTAuMDE2MjI4MDc2NzYzODAzNDQ1JnJvbGU9cHVibGlzaGVyJmV4cGlyZV90aW1lPTE2NDM3OTA1MDUmaW5pdGlhbF9sYXlvdXRfY2xhc3NfbGlzdD0=",
        "T1==cGFydG5lcl9pZD00NzQxMDgwMSZzaWc9YTI4MTRmZDNjZDA5NzE2MDA4ODM2ZTcyOTRmOWQ4NTg0NDNhMDJjZTpzZXNzaW9uX2lkPTFfTVg0ME56UXhNRGd3TVg1LU1UWTBNVEU1TmpZek5EZzVOMzVsYUVONGFISnpVa3Q1YmtGUloyNXpUMHR4TWxwbU5YbC1mZyZjcmVhdGVfdGltZT0xNjQxMzY2MjYyJm5vbmNlPTAuMjM3NzA3MzcyNTE4Mjc3OSZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNjQzOTU4MjYwJmluaXRpYWxfbGF5b3V0X2NsYXNzX2xpc3Q9",
        "T1==cGFydG5lcl9pZD00NzQxMDgwMSZzaWc9ZjJjZDA2MjQ4OTQ4Mzc4NzU0OGQ1ZDAyNjkzYzhkY2RlM2ViZTA2NDpzZXNzaW9uX2lkPTFfTVg0ME56UXhNRGd3TVg1LU1UWTBNVEU1TmpZek5EZzVOMzVsYUVONGFISnpVa3Q1YmtGUloyNXpUMHR4TWxwbU5YbC1mZyZjcmVhdGVfdGltZT0xNjQxMzYzMTI0Jm5vbmNlPTAuODI1MTQ2MDkwNzkyNTIyNSZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNjQzOTU1MTIzJmluaXRpYWxfbGF5b3V0X2NsYXNzX2xpc3Q9",
        "T1==cGFydG5lcl9pZD00NzQxMDgwMSZzaWc9ODY3ZGExOGNjOTQ1MmI0OGVhNGI3ZmZjNTZmOWM1MWEyMjkwNmFlZjpzZXNzaW9uX2lkPTFfTVg0ME56UXhNRGd3TVg1LU1UWTBNVEU1TmpZek5EZzVOMzVsYUVONGFISnpVa3Q1YmtGUloyNXpUMHR4TWxwbU5YbC1mZyZjcmVhdGVfdGltZT0xNjQxMzc2MTA1Jm5vbmNlPTAuMjY1MDE2NDQwOTc3MDY4NTYmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTY0Mzk2ODEwMSZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ=="
    )

    val SUBSCRIBE_TOKEN = arrayListOf<String>(
        "T1==cGFydG5lcl9pZD00NzQxMDgwMSZzaWc9ZTMzMGRjZTU0M2IxODQzOTA1Y2MwZWYwYWNhOTA5YmM5NDhjZDNhZDpzZXNzaW9uX2lkPTFfTVg0ME56UXhNRGd3TVg1LU1UWTBNVEU1TmpZek5EZzVOMzVsYUVONGFISnpVa3Q1YmtGUloyNXpUMHR4TWxwbU5YbC1mZyZjcmVhdGVfdGltZT0xNjQxMTk4NTIxJm5vbmNlPTAuMTE1NDY4MTA4NzExODI3ODkmcm9sZT1zdWJzY3JpYmVyJmV4cGlyZV90aW1lPTE2NDM3OTA1MjAmaW5pdGlhbF9sYXlvdXRfY2xhc3NfbGlzdD0=",
        "T1==cGFydG5lcl9pZD00NzQxMDgwMSZzaWc9NDAyNmMzOWEyNTMxMmMxMzBiZWJjMmFkNWFhYzM5NDc1ZjhhODNjZDpzZXNzaW9uX2lkPTFfTVg0ME56UXhNRGd3TVg1LU1UWTBNVEU1TmpZek5EZzVOMzVsYUVONGFISnpVa3Q1YmtGUloyNXpUMHR4TWxwbU5YbC1mZyZjcmVhdGVfdGltZT0xNjQxMTk4NTMxJm5vbmNlPTAuODk2MTE4NjU2NzUzNjIwMyZyb2xlPXN1YnNjcmliZXImZXhwaXJlX3RpbWU9MTY0Mzc5MDUzMCZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ==",
        "T1==cGFydG5lcl9pZD00NzQxMDgwMSZzaWc9OGUyMTQ3OTk0OWY5YmFhMmY3NjNmZDg4ZjEzYTliMTkyYzFmMDg5NDpzZXNzaW9uX2lkPTFfTVg0ME56UXhNRGd3TVg1LU1UWTBNVEU1TmpZek5EZzVOMzVsYUVONGFISnpVa3Q1YmtGUloyNXpUMHR4TWxwbU5YbC1mZyZjcmVhdGVfdGltZT0xNjQxMTk4NTUwJm5vbmNlPTAuOTU2Njc2MjU4MjM1ODIxNyZyb2xlPXN1YnNjcmliZXImZXhwaXJlX3RpbWU9MTY0Mzc5MDU0OSZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ==",
        "T1==cGFydG5lcl9pZD00NzQxMDgwMSZzaWc9ZWE5ZjYxOWEwYzVmMzc1N2ExZWMxYTZlOGVjOTE2NTNjNzNmMjA1YjpzZXNzaW9uX2lkPTFfTVg0ME56UXhNRGd3TVg1LU1UWTBNVEU1TmpZek5EZzVOMzVsYUVONGFISnpVa3Q1YmtGUloyNXpUMHR4TWxwbU5YbC1mZyZjcmVhdGVfdGltZT0xNjQxMTk4NTYwJm5vbmNlPTAuMjIwMjY5ODk2NDk1Nzg5NjUmcm9sZT1zdWJzY3JpYmVyJmV4cGlyZV90aW1lPTE2NDM3OTA1NTkmaW5pdGlhbF9sYXlvdXRfY2xhc3NfbGlzdD0="
    )

    val MODERATOR_TOKEN = arrayListOf<String>(
        "T1==cGFydG5lcl9pZD00NzQxMDgwMSZzaWc9MzkxMGI2NTFhZGM5Nzg4NmVjMzIwMWY1ZTRhOWY3NGU4ZThmNTQ3ODpzZXNzaW9uX2lkPTFfTVg0ME56UXhNRGd3TVg1LU1UWTBNVEU1TmpZek5EZzVOMzVsYUVONGFISnpVa3Q1YmtGUloyNXpUMHR4TWxwbU5YbC1mZyZjcmVhdGVfdGltZT0xNjQxMzYxMzgyJm5vbmNlPTAuNTkwOTI4MDI3NDg2ODA5MiZyb2xlPW1vZGVyYXRvciZleHBpcmVfdGltZT0xNjQzOTUzMzgxJmluaXRpYWxfbGF5b3V0X2NsYXNzX2xpc3Q9"
    )

    // *** The code below is to validate this configuration file. You do not need to modify it  ***
    val isValid: Boolean
        get() = !(TextUtils.isEmpty(API_KEY) || TextUtils.isEmpty(SESSION_ID))

    val description: String
        get() = """
               OpenTokConfig:
               API_KEY: $API_KEY
               SESSION_ID: $SESSION_ID
               TOKEN: $PROVIDER_TOKEN
               """.trimIndent()


}